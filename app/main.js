const nav = document.querySelector("nav h1").addEventListener("click", () => {
  location.hash = "#";
});
const hamburger = document.querySelector("#hamburger");
const hamburgerMenu = document.querySelector("#hamburger-menu");
const hummer = document.querySelector("#hummer")
hummer.addEventListener("click",()=> location.hash = "auction")
function handleRoute() {
  let hash = location.hash;
 
  styles(hash);
  
  const allSections = document.querySelectorAll("section");

  allSections.forEach((section) => (section.style.display = "none"));
  

  if (hash) {
    document.querySelector(hash).style.display = "block";
  }

  switch (hash) {
    // case '#':
    //     initLandingPage()
    //     break;
    case "#visitorHomePage":
      initVisitorHomePage();
      break;
    case "#visitorListing":
      initVisitorListing();
      break;
    case "#artistHomePage":
      initArtistHomePage();
      break;
    case "#artistListing":
      initArtistListing();
      break;
    case "#auction":
      initAuction();
      break;
    default:
      initLandingPage();
      document.querySelector("#landingPage").style.display = "block";
  }
}

function styles(hash) {
  const h1 = document.querySelector("nav h1");
h1.innerHTML = "Street Artist"

  const logo = document.querySelector("nav img");

  hash === "" ? logo.classList.add("d-none") : logo.classList.remove("d-none");
  const artistName = document.querySelector("nav h1");

  hash.includes("artist")
    ? artistName.classList.add("ml-h1")
    : artistName.classList.remove("ml-h1");
  hash.includes("artist")
    ? hamburger.classList.remove("d-none")
    : hamburger.classList.add("d-none");

    hash.includes("visitor")
    ? hummer.classList.remove("d-none") 
    : hummer.classList.add("d-none");

    hash.includes("visitor")
    ? artistName.classList.add("ml-h1")
    : artistName.classList.remove("ml-h1");
}

hamburger.addEventListener("click", () => {
  hamburgerMenu.classList.toggle("d-none");
});

window.addEventListener("hashchange",()=>{
handleRoute()
hamburgerMenu.classList.contains("d-none")? console.log() :hamburgerMenu.classList.add("d-none")

} );
window.addEventListener("load", handleRoute);
