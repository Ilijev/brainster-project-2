function initArtistHomePage(){

const navH1 =document.querySelector("nav h1")
navH1.innerHTML = loggedUser
navH1.classList.add("ml-h1")

const itemsSold = document.querySelector("#items-sold")
const allItems = document.querySelector("#all-items")
const totalIncome = document.querySelector("#total-income")

    const userItems =JSON.parse(itemLS).filter(item =>  item.artist == loggedUser)
    let sum =0 ;
    userItems.filter(item => item.dateSold).map(item => sum += item.priceSold)
    itemsSold.innerHTML = userItems.filter(item => item.dateSold).length
    allItems.innerHTML = userItems.length
    totalIncome.innerHTML = sum+"$";
    // console.log(userItems.filter(item => item.dateSold).map(item => sum += item.priceSold));
function generateDates(daysAgo) {


    const arr = []

    for (let i = 0; i < daysAgo; i++) {
        const start = new Date()
        const startDate = start.getDate()
        const currentDate = start.setDate(startDate - i)
        const formatted = formatDate(currentDate)
        arr.push(formatted)
    }


    return arr
}

function formatDate(date) {
    const d = new Date(date)
    return new Date(date).toLocaleDateString()
}


    const labels = [
        generateDates(7)
    ];

    const data = {
        labels: labels,
        datasets: [{
            label: 'Amount',
            backgroundColor: 'rgba(161, 106, 94, 1)',
            borderColor: 'rgb(255, 99, 132)',
            data: [0, 10, 5, 2, 20, 30, 45],
        }]
    };

    const config = {
        type: 'bar',
        data: data,
        options: {
            indexAxis: 'y',
        }
    };

    
   
    const myChart = new Chart(
        document.getElementById('myChart'),
        config
    );
    

    const last7 = document.querySelector('#last7')
    const last14 = document.querySelector('#last14')
    const last30 = document.querySelector('#last30')

   
    const soldByArtistItems = JSON.parse(itemLS).filter(item => (item.artist === loggedUser.value) && !!item.priceSold)
        


    last7.addEventListener('click', function () {
        const labels = generateDates(7)
        myChart.data.labels = labels


        const newData = labels.map(label => {
            // console.log(label)

            let sum = 0

            soldByArtistItems.forEach(item => {
                if (formatDate(item.dateSold) === label) {
                    sum += item.priceSold
                }
            })

            return sum

        })



        myChart.data.datasets[0].data = newData
        myChart.update()
    })


    last14.addEventListener('click', function () {
        const labels = generateDates(14)
        myChart.data.labels = labels


        const newData = labels.map(label => {
            // console.log(label)

            let sum = 0

            soldByArtistItems.forEach(item => {
                if (formatDate(item.dateSold) === label) {
                    sum += item.priceSold
                }
            })

            return sum

        })



        myChart.data.datasets[0].data = newData
        myChart.update()
    })


    last30.addEventListener('click', function () {
        const labels = generateDates(30)
        myChart.data.labels = labels


        const newData = labels.map(label => {
            // console.log(label)

            let sum = 0

            soldByArtistItems.forEach(item => {
                if (formatDate(item.dateSold) === label) {
                    sum += item.priceSold
                }
            })

            return sum

        })



        myChart.data.datasets[0].data = newData
        myChart.update()
    })


    // const hamburger = document.querySelector("#hamburger")
    // hamburger.addEventListener("click",()=>{
    //     console.log("clicked");
    // })



}