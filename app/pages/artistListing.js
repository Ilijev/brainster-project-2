function initArtistListing() {
  const navH1 =document.querySelector("nav h1")
navH1.innerHTML = loggedUser
navH1.classList.add("ml-h1")

  const artistListContainer = document.querySelector("#artistListContainer");
  const updateBtn = document.querySelector("#update-btn");
  const filterTitle = document.querySelector("#filter-title");
  // console.log(loggedUser.value);
  const newItemForm = document.querySelector("#new-item-form");

  let art = JSON.parse(itemLS).filter((item) => item.artist.includes(loggedUser));
  let item;
  function renderArtistCard(card, container) {
    card.forEach((item) => {
      container.innerHTML += `<div class="card border-0 bg-light my-3" data-id="${
        item.id
      }">
        <img src="${item.image}" />
        <div class="p-2">
          <div class="d-flex justify-content-between align-items-center">
            <span class="  p-0">${item.title}</span>
            <span class="p-0 pc-default px-1 text-light">$${item.price}</span>
          </div>
          <h5 class="card-title pc-default-text fs-6">${item.dateSold}</h5>
          <p class="card-text fs-7">${item.description}</p>
        </div>
        <div class="d-flex justify-content-around p-2 pc-default">
          <button class="btn btn-primary p-1 rounded-1 auctionItem fs-7 fw-normal">Send to auction</button>
          <button class="btn published-btn ${
            item.isPublished ? "bg-success" : "bg-danger"
          } p-1 text-white rounded-1 fs-7 fw-normal" ">${
        item.isPublished ? "Published" : "Unpublished"
      }</button>
          <button class="btn btn-danger p-1 rounded-1 remove-item fs-7 fw-normal">Remove</button>
          <button class="btn btn-danger p-1 rounded-1 edit-btn fs-7 fw-normal">Edit</button>
        </div>
      </div>`;
    });
  }
  window.addEventListener("click", (e) => {
    // e.target.classlist.contains("remove-item").parentElement.parentElement.remove()
    // console.log(e.target?.classList?.contains("remove-item"));
    item = art.find(
      (item) =>
        item.id == e.target.parentElement.parentElement.getAttribute("data-id")
    );
    if (e.target?.classList?.contains("remove-item")) {
      e.target.parentElement.parentElement.remove();
      art = art.filter((itemIn) => itemIn.id !== item.id);
    }

    if (e.target?.classList?.contains("published-btn")) {
      item.isPublished = !item.isPublished;
      artistListContainer.innerHTML = "";
      const temp = JSON.parse(itemLS).map(it => {
        if(it.id == e.target.parentElement.parentElement.getAttribute("data-id")){
          it = item
        }
        return it
      })
      
      localStorage.setItem("itemsLS", JSON.stringify(temp));
    }

    if (e.target?.classList?.contains("edit-btn")) {
      // isEditing = true;
      filterTitle.innerHTML = "Editing item";
      addNew.classList.add("d-none");
      updateBtn.classList.remove("d-none");
      displayArtForm();

      e.target.classList.add("alen");
    }

    if(e.target?.classList?.contains("auctionItem")){
      auctioningItem = item
      console.log(auctioningItem);
      item.isAuctioning = true;
      location.hash = "auction"
    }

    renderArtistCard(art, artistListContainer);
  });
  cancleBtn = document.querySelector("#cancle-btn");
  addNew = document.querySelector("#add-btn");
  addNewItem = document.querySelector("#add-New-Item");

  cancleBtn.addEventListener("click", () => {
    addNewItem.classList.toggle("d-none");

    newItemForm.classList.toggle("d-none");
    artistListContainer.classList.toggle("d-none");
  });

  let published = document.querySelector("#published");
  const titleArtist = document.querySelector("#title-artist");
  const descArtist = document.querySelector("#desc-artist");
  const artType = document.querySelector("#art-type");
  const artPrice = document.querySelector("#art-price");
  const imgUrl = document.querySelector("#img-url");

  // console.log(item);

  addNewItem.addEventListener("click", () => {
    // isEditing = false
    console.log("asdij");
    updateBtn.classList.add("d-none");
    addNew.classList.remove("d-none");
    filterTitle.innerHTML = "Add new item";

    displayArtForm();
  });

  function displayArtForm() {
    addNewItem.classList.toggle("d-none");

    newItemForm.classList.toggle("d-none");
    artistListContainer.classList.toggle("d-none");
  }

  addNew.addEventListener("click", () => {
    let newItem = {
      id: new Date().valueOf(),
      title: titleArtist.value,
      description: descArtist.value,
      type: artType.value,
      image: imgUrl.value,
      price: artPrice.value,
      artist: "Leanne Graham",
      dateCreated: new Date(),
      isPublished: published.checked,
      isAuctioning: false,
      dateSold: "2022-10-11T15:21:41.926Z",
      priceSold: 2030,
    };

    art.unshift(newItem);
    let temp = JSON.parse(itemLS);
    temp.unshift(newItem);
    localStorage.setItem("itemsLS", JSON.stringify(temp));
    artistListContainer.innerHTML = "";
    renderArtistCard(art, artistListContainer);
    displayArtForm();
  });

  function updateItem(item) {
    const idx = items.indexOf(item);
    items[idx] = item;
    //local storage update
  }

  updateBtn.addEventListener("click", () => {
    const alen = document.querySelector(".alen");
    const itemId = alen.parentElement.parentElement.getAttribute("data-id");
    item = art.find((artistItem) => artistItem.id == itemId);
    alen.classList.remove("alen");
    item.title = titleArtist.value;
    item.description = descArtist.value;
    item.type = artType.value;
    item.price = artPrice.value;
    item.image = imageUrl? imageUrl: imgUrl;

    const temp = JSON.parse(itemLS).map(it => {
      if(it.id == itemId){
        it = item
      }
      return it
    })
    
    localStorage.setItem("itemsLS", JSON.stringify(temp));
    artistListContainer.innerHTML = "";
    displayArtForm();
     
  });
  renderArtistCard(art, artistListContainer);

  const editCanvas = document.querySelector("#edit-canvas");

  editCanvas.addEventListener("click", () => {
    stream();
    
    document.querySelector("#cameraPage").classList.remove("d-none");

    document.querySelector("#artist-form").classList.add("d-none");
    
    
  });
// img capturing
  let isCapturing = false;
  
  let imageUrl;
  function stream() {
    const video = document.querySelector("#liveCamera");

    console.log("capture image");
    const captureImageBtn = document.querySelector("#captureImageBtn");
    const stopStreamBtn = document.querySelector("#stopStream");
  
    const canvas = document.querySelector("#capture");
    const image = document.querySelector("#capturedImage");
  
  console.log(isCapturing);
    

    if (!isCapturing) {
      navigator.mediaDevices
        .getUserMedia({
          video: {
            facingMode: { ideal: "environment" },
          },
        })
        .then((stream) => {
          video.srcObject = stream;
          isCapturing = true;
        });
  // console.log(isCapturing);
        
      video.addEventListener("canplay", function () {
        video.width = video.videoWidth;
        video.height = video.videoHeight;

        canvas.width = video.videoWidth;
        canvas.height = video.videoHeight;
      });
    }
    captureImageBtn.removeEventListener("click", () => {});
   
    captureImageBtn.addEventListener("click", function () {
      
      document.querySelector("#cameraPage").classList.add("d-none");
      document.querySelector("#artist-form").classList.remove("d-none");
      const ctx = canvas.getContext("2d");
      
      
      ctx.drawImage(video, 0, 0);
      imageUrl = ""
      imageUrl = canvas.toDataURL("image/png");
      console.log(imageUrl);
      editCanvas.style.background= `url("${imageUrl}")`;
     
      // image.src = imageUrl
      
    });
    captureImageBtn.addEventListener("click", function () {
      const stream = video.srcObject;
      ctx.fillRect(0, 0, width, height);
      const tracks = stream.getTracks();

      console.log(tracks);

      tracks.forEach((track) => {
        track.stop();
      });
      isCapturing = false
      
    });
    
  }
}
