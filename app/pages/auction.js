function initAuction() {
  const auctioningImg = document.querySelector("#auctioningImg");
  const bidAmount = document.querySelector("#bidAmount");
  const results = document.querySelector("#biddings");
  const timer = document.querySelector("#countDown");
  const bidFromAuction = document.querySelector("#bidFromAuction");
  const finishBitting = document.querySelector("#finishBitting");
  finishBitting.classList.add("d-none");

  // let isAuctioning = items.filter(item => item.isAuctioning )

  let win;
  let winPrice;

  let old_element = document.querySelector("#bidBtn");
  let new_element = old_element.cloneNode(true);
  old_element.parentNode.replaceChild(new_element, old_element);

  if(localStorage.getItem('artist') === 'visitor'){
    new_element.disabled = true;
  }
  
  function setPriceSold(){
    let tempItems = JSON.parse(itemLS);
    tempItems.map(it => {
      if(it.id === auctioningItem.id){
        it.priceSold = winPrice;
      }
      return it
    })

    console.log(tempItems);
    localStorage.setItem('itemsLS',JSON.stringify(tempItems))
  }

  new_element.addEventListener("click", async function () {
    if (!auctioningItem) {
      alert("There is no item up for auction at the moment");
      location.hash = "artistListing";
      return;
    }

    let timeleft = 30;
    let downloadTimer = setInterval(function () {
      timer.innerHTML = timeleft + "&nbsp" + "seconds remaining";
      new_element.addEventListener("click", () => {
        clearInterval(downloadTimer);
      });
      timeleft -= 1;
      if (timeleft == 0) {
        setPriceSold()
        clearInterval(downloadTimer);
        timer.innerHTML = "Time is up!";
      }
    }, 1000);

    results.innerHTML += `<li class='my-bid fs-5 '>${bidAmount.value}</li>`;

    const res = await fetch("https://blooming-sierra-28258.herokuapp.com/bid", {
      method: "POST",
      body: JSON.stringify({ amount: bidAmount.value }),
      headers: {
        "Content-Type": "application/json",
      },
    });

    const data = await res.json();

    if (data.isBidding) {
      bidAmount.min = data.bidAmount + 20;
      bidAmount.value = data.bidAmount + 20;
      results.innerHTML += `<li class='away-bid fs-5'>${data.bidAmount}</li>`;
      bidFromAuction.innerHTML += data.bidAmount;
    } else {
      results.innerHTML += `<li class='away-bid'>Congratz You win!</li>`;
      bidFromAuction.innerHTML += bidAmount.value;
      auctioningItem.priceSold = results ? data.bidAmount : bidAmount.value;
      
      winPrice = bidAmount.value;

      setPriceSold()
      auctioningItem.isAuctioning = false;
      auctioningItem = undefined;
      timer.innerHTML = "";
      clearInterval(downloadTimer);

      new_element.removeEventListener("click", () => {});
      finishBitting.classList.remove("d-none");
      finishBitting.addEventListener("click", () => {
        results.innerHTML = "";
        location.hash = "artistListing";
      });
    }
  });
  // bidFromAuction.innerHTML = win;
}
