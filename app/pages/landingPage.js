
function initLandingPage() {
   

    const btn = document.querySelector('#join-as-visitor')
    btn.removeEventListener('click', onClick)    
    btn.addEventListener('click',onClick )

   
    
    handleRoute
}

function onClick() {
    location.hash += "visitorHomePage"
    localStorage.setItem('artist', 'visitor')
}


fetch("https://jsonplaceholder.typicode.com/users")
.then(function(response){ return response.json() }) 
.then(function(data){ 
    const selecteArtist = document.querySelector("#select-artist")
    
    // console.log(data) 
    let artists = data.map(element => element.name);

    artists.forEach(artist => {
        let option =document.createElement("option")
        option.innerText = artist
        selecteArtist.append(option)
    });

    

    selecteArtist.addEventListener("change",()=>{
        location.hash = "artistHomePage"
        localStorage.setItem("artist",selecteArtist.value)

    })
}); 


