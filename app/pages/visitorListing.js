function initVisitorListing() {
  const cardConainer = document.querySelector("#cardContainer");
  const filterForm = document.querySelector("#filter-form");
  const openFilter = document.querySelector("#open-filter");
  
  function renderVisitorCard(card, container) {
    
    card
      .filter((item) => item.isPublished)
      .forEach((item,idx) => {
        container.innerHTML += `<div class="card border-0 ${idx % 2 == 0? "bg-light": "pc-default"} my-3">
          <img src="${item.image}" />
          <div class="p-2">
            <div class="d-flex justify-content-between align-items-center">
              <h5 class="card-title styled-text ${idx % 2 == 0? "pc-default-text": "text-light"} p-0">${item.artist}</h5>
              <span class="p-0 px-1 ">$${item.price}</span>
            </div>
            <h5 class="card-title ${idx % 2 == 0? "pc-default-text": "text-light"} fs-6">${item.title}</h5>
            <p class="card-text ${idx % 2 == 0? "pc-default-text": "text-light"} fs-7">${item.description}</p>
          </div>
        </div>`;
      });

    }

  fetch("https://jsonplaceholder.typicode.com/users")
    .then(function (response) {
      return response.json();
    })
    .then(function (data) {
      const selecteArtist = document.querySelector("#filter-artist");

      let artists = data.map((element) => element.name);

      artists.forEach((artist) => {
        let option = document.createElement("option");
        option.innerText = artist;
        selecteArtist.append(option);
      });
    });

  const artType = document.querySelector("#art-type");
  itemTypes.forEach((item) => {
    let option = document.createElement("option");
    option.innerText = item;
    artType.append(option);
  });

  const filterSubmit = document.querySelector("#filter-submit");

  filterSubmit.addEventListener("click", (e) => {
    e.preventDefault();

    let titleFilter = document.querySelector("#title-filter").value;
    let filterArtist = document.querySelector("#filter-artist").value;
    let minPrice = document.querySelector("#min-price").value;
    let maxPrice = document.querySelector("#max-price").value;
    let artTypeNew = document.querySelector("#art-type").value;

    // console.log("min price",minPrice);

    // filtered = items.filter((item) =>
    //   item.title.toLowerCase().includes(titleFilter.toLowerCase().value) &&
    //   filterArtist.value ? item.artist.toLowerCase().includes(filterArtist.value) : true &&
    //   minPrice ? item.price <= minPrice : true &&
    //   maxPrice ? item.price >= maxPrice : true &&
    //   artTypeNew ? item.type === byType : true
    // );
    // console.log(filtered);
    // cardConainer.innerHTML= ""
    // renderCard(filtered,cardConainer)

    const publishedItems = JSON.parse(itemLS).filter((item) => item.isPublished);

    let filtered = publishedItems.filter(
      (item) =>
        (titleFilter
          ? item.title.toLowerCase().includes(titleFilter.toLowerCase())
          : true) &&
        (minPrice ? item.price >= minPrice : true) &&
        (maxPrice ? item.price <= maxPrice : true) &&
        (artTypeNew ? item.type === artTypeNew : true) &&
        (filterArtist ? item.artist === filterArtist : true)
    );
    console.log(filtered);
    cardConainer.innerHTML = "";
    filterForm.classList.add("d-none");
    cardConainer.style.display = "block";
    openFilter.style.display = "block";



    renderVisitorCard(filtered, cardConainer);
  });

  renderVisitorCard(JSON.parse(itemLS), cardConainer);

  openFilter.addEventListener("click", (e) => {
    e.preventDefault();
    filterForm.classList.remove("d-none");
    cardConainer.style.display = "none";
    openFilter.style.display = "none";
  });
  const xBtn = document.querySelector("#close-filter");
  xBtn.addEventListener("click", (e) => {
    e.preventDefault();
    filterForm.classList.add("d-none");
    cardConainer.style.display = "block";
    openFilter.style.display = "block";
  });
}
